#!/bin/bash

FILE=vcpkg_dependencies
CMAKE_TOOLCHAIN_ARG=""
if test -f "$FILE"; then
#    echo "$FILE exists"
    vcpkg_dependencies=$(cat $FILE)
    VcpkgDir="${VCPKG_DIR:-../libs/vcpkg}"
    CMAKE_TOOLCHAIN_ARG="-DCMAKE_TOOLCHAIN_FILE=$VcpkgDir/scripts/buildsystems/vcpkg.cmake"
    
    echo "Using VCPKG DIR: $VcpkgDir"
    # init vcpkg
    cd $VcpkgDir
    
    if [ ! -f vcpkg ]; then
      echo "vcpkg does not exist - building"
      if [ ! -f bootstrap-vcpkg.sh ]; then
        git submodule update --init
      fi
      ./bootstrap-vcpkg.sh
    fi
    ./vcpkg install $vcpkg_dependencies
    cd -
else
    echo "Did not find vcpkg_dependencies file. Not building/installing vcpkg packages"
fi

# handle additional cmake params
PARAM_FILE=cmake_params
CMAKE_PARAM_ARGS=""
if test -f "$PARAM_FILE"; then
#    echo "$PARAM_FILE exists"
    CMAKE_PARAM_ARGS=$(cat $PARAM_FILE)
#    echo "$PARAM_FILE exists: CMAKE_PARAM_ARGS:$CMAKE_PARAM_ARGS"
else
    echo "Did not find cmake_params file. "
fi

cd ..
# build 
mkdir -p build
cd build

cmake "$CMAKE_TOOLCHAIN_ARG" "$CMAKE_PARAM_ARGS" ..
exit_status=$?
if [ $exit_status -ne 0 ]; then
	echo "Failed to generate CMake build files: $(pwd)"
    exit 1
fi


cmake --build . --config Release
exit_status=$?

cd ../scripts  

exit $exit_status

