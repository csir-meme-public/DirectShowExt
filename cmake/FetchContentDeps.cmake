include(FetchContent)

FetchContent_Declare(
    baseclasses
    GIT_REPOSITORY https://gitlab.com/csir-meme/baseclasses.git
    GIT_TAG        master
)

FetchContent_MakeAvailable(baseclasses)
